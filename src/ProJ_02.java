import java.util.Scanner;

public class ProJ_02 {
	
	static String rac [][] = {{" ","1","2","3"},
							{"1","-","-","-"},
							{"2","-","-","-"},
							{"3","-","-","-"}};
	static String player = "o";
	static int row,col,count = 0;
	
	
	private static void printWelcome() {
		System.out.println("Welcome to XO Game");
	}
	private static void printTable() {
		for (int i = 0; i < rac.length; i++) {
			for (int j = 0; j < rac.length; j++) {
				System.out.print(rac[i][j]+ " ");
			}
			System.out.println();
		}
		
	}
	private static void printShowturn() {
	
		if (player == "o") {
			System.out.println("Turn O");
		}else {
			System.out.println("Turn X");
		}
	}
	private static void input() {
		Scanner kb = new Scanner(System.in);
		if (player == "o") {
			System.out.print("O(R,C): ");
			row = kb.nextInt();
			col = kb.nextInt();
			if ( row <= 3 && row >= 1 && col <= 3 && col >= 1 && rac[row][col] == "-") {
				rac[row][col] = "o";
			}else {
				System.out.println("INPUT AGAIN!!!");
				input();
			}
		}else {
			System.out.print("X(R,C): ");
			row = kb.nextInt();
			col = kb.nextInt();
			if ( row <= 3 && row >= 1 && col <= 3 && col >= 1 && rac[row][col] == "-") {
				rac[row][col] = "x";
			}else {
				System.out.println("INPUT AGAIN!!!");
				input();
			}
		}
	}
	private static void Switchplayer() {
		if (player == "o") {
			player = "x";
		}else {
			player = "o";
		}
		count++;
	}
	private static boolean checkWin() {
		 	if (rac[1][1].equals("o") && rac[1][2].equals("o") && rac[1][3].equals("o")) {
		 		return true;
			}if (rac[2][1].equals("o") && rac[2][2].equals("o") && rac[2][3].equals("o")) {
		 		return true;
			}if (rac[3][1].equals("o") && rac[3][2].equals("o") && rac[3][3].equals("o")) {
		 		return true;
			}if (rac[1][1].equals("o") && rac[2][1].equals("o") && rac[3][1].equals("o")) {
		 		return true;
			}if (rac[1][2].equals("o") && rac[2][2].equals("o") && rac[3][2].equals("o")) {
		 		return true;
			}if (rac[1][3].equals("o") && rac[2][3].equals("o") && rac[3][3].equals("o")) {	 
		 		return true;
			}if (rac[1][1].equals("o") && rac[2][2].equals("o") && rac[3][3].equals("o")) {	 
		 		return true;
			}if (rac[3][1].equals("o") && rac[2][2].equals("o") && rac[1][3].equals("o")) {
		 		return true;
			}if (rac[1][1].equals("x") && rac[1][2].equals("x") && rac[1][3].equals("x")) {
		 		return true;
			}if (rac[2][1].equals("x") && rac[2][2].equals("x") && rac[2][3].equals("x")) {
		 		return true;
			}if (rac[3][1].equals("x") && rac[3][2].equals("x") && rac[3][3].equals("x")) {
		 		return true;
			}if (rac[1][1].equals("x") && rac[2][1].equals("x") && rac[3][1].equals("x")) {
		 		return true;
			}if (rac[1][2].equals("x") && rac[2][2].equals("x") && rac[3][2].equals("x")) {
		 		return true;
			}if (rac[1][3].equals("x") && rac[2][3].equals("x") && rac[3][3].equals("x")) {	 
		 		return true;
			}if (rac[1][1].equals("x") && rac[2][2].equals("x") && rac[3][3].equals("x")) {	 
		 		return true;
			}if (rac[3][1].equals("x") && rac[2][2].equals("x") && rac[1][3].equals("x")) {
		 		return true;
			}
		return false;
	}
	private static void showValue() {
		if (count % 2 == 0 && count < 9) {
			System.out.println("Player X Winner!!!");
		}else if (count % 2 == 1 && count < 9) {
			System.out.println("Player O Winner!!!");
		}else if (count == 9) {
			System.out.println("Draw!!!");
		}
	}
	private static boolean checkDraw() {
		if (count == 10) {
			return true;
		}
		return false;
	}
	private static void showBye() {
		System.out.println("Bye Bye!!!");
	}
	
	public static void main(String[] args) {
		printWelcome();
		printTable();
		while (true) {
			printShowturn();
			input();
			Switchplayer();
			printTable();
			if (checkWin() == true) {
				break;
			}if (checkDraw() == true) {
				break;
			}
		}
		showValue();
		showBye();
	}
	
}
